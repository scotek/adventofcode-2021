module DayN

import System
import AOCUtils
import Data.List


task1 : List String -> Maybe String


task2 : List String -> Maybe String


{-
-- 
-}
main1 : IO()
main1 = withLines' "day0n-input.txt" task1

{-
-- 
-}
main2 : IO()
main2 = withLines' "day0n-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
