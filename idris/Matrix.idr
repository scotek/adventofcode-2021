||| Matrix implementation since it no longer seems to appear in Idris2 standard library.
module Matrix

import Data.Vect
import Data.List

||| Type of a matrix.
public export
Matrix : (rows : Nat) -> (cols : Nat) -> (element : Type) -> Type
Matrix r c el = Vect r (Vect c el)

||| Create an `r` x `n` matrix of a given value.
export
replicateMatrix : (r:Nat) -> (c:Nat) -> a -> Matrix r c a
replicateMatrix r c x = replicate r (replicate c x)

||| Replace a value at a specific location.
export
replaceAt : Matrix r c a -> (r' : Fin r) -> (c' : Fin c) -> a -> Matrix r c a
replaceAt m r c x = let inner = index r m
                    in replaceAt r (replaceAt c x inner) m

||| Get a value at a specific location.
export
index : Matrix r c a -> (r' : Fin r) -> (c' : Fin c) -> a
index m r c = index c $ index r m

||| Return the first position for which the predicate is true.
export
findIndex : (a -> Bool) -> Matrix r c a -> Maybe (Fin r, Fin c)
findIndex p m = let rows = map (findIndex p) m
                    rowMatch = findIndex isJust rows
                in case rowMatch of
                        Nothing => Nothing
                        Just match => case index match rows of
                                           Nothing => Nothing  -- Always Just since match comes from findIndex
                                           Just c' => Just (match, c')

||| Transpose the matrix.
export
transposeMatrix : {c:Nat} -> Matrix r c a -> Matrix c r a
transposeMatrix [] = replicate c []
transposeMatrix (x :: xs) = let xs' = transposeMatrix xs
                            in zipWith (::) x xs'

||| Fold across the rows of the matrix.
export
foldRows : (a -> b -> b) -> b -> Matrix r c a -> Vect r b
foldRows f acc m = map (foldr f acc) m

||| Fold down the columns of the matrix.
export
foldColumns : {c:Nat} -> (a -> b -> b) -> b -> Matrix r c a -> Vect c b
foldColumns f acc m = foldRows f acc (transposeMatrix m)

||| Create a matrix of given dimensions from a nested list of values,
||| coverting each element with a function.
export
toMatrixWith : (a -> b) -> (r : Nat) -> (c : Nat) ->  List (List a) -> Maybe (Matrix r c b)
toMatrixWith fn r c xs = let xs' = catMaybes $ map (convRow c) xs
                         in toVect r xs'
                         where convRow : (c : Nat) -> List a -> Maybe (Vect c b)
                               convRow c xs = (Just (map fn)) <*> (toVect c xs)
                               
||| Create a matrix of given dimensions from a nested list of values.
export
toMatrix : (r : Nat) -> (c : Nat) -> List (List a) -> Maybe (Matrix r c a)
toMatrix = toMatrixWith id
