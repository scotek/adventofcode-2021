module Day02

import Data.List
import System
import AOCUtils

import Data.String

data Movement = Forward Integer
              | Down Integer
              | Up Integer

parseMovementLine : String -> Maybe Movement
parseMovementLine str
  = let parts = words str
        ans = case inBounds 1 parts of
                   Yes prf => case inBounds 0 parts of
                                   Yes prf' => case index 0 parts of
                                                    "forward" => Just $ Forward $ cast (index 1 parts)
                                                    "down" => Just $ Down $ cast (index 1 parts)
                                                    "up" => Just $ Up $ cast (index 1 parts)
                                                    otherwise => Nothing
                                   No _ => Nothing
                   No _ => Nothing
    in ans

||| Process the moves for task 1 keeping track of just the horizontal position and depth.
execMoves : List Movement -> (Integer, Integer) -> (Integer, Integer)
execMoves [] pos = pos
execMoves (Forward n :: xs) (horiz, depth) = execMoves xs (horiz + n, depth)
execMoves (Down n :: xs) (horiz, depth) = execMoves xs (horiz, depth + n)
execMoves (Up n :: xs) (horiz, depth) = execMoves xs (horiz, depth - n)


task1 : List String -> Maybe Integer
task1 strs = let cmds = catMaybes $ map parseMovementLine strs
                 (h, d) = execMoves cmds (0,0)
             in Just (h * d)



||| Process the moves for task 2 keeping track of the horizontal position, depth, and now aim.
execMoves' : List Movement -> (Integer, Integer, Integer) -> (Integer, Integer, Integer)
execMoves' [] pos = pos
execMoves' (Forward n :: xs) (horiz, depth, aim) = execMoves' xs (horiz + n, depth + (aim * n), aim)
execMoves' (Down n :: xs) (horiz, depth, aim) = execMoves' xs (horiz, depth, aim + n)
execMoves' (Up n :: xs) (horiz, depth, aim) = execMoves' xs (horiz, depth, aim - n)


task2 : List String -> Maybe Integer
task2 strs = let cmds = catMaybes $ map parseMovementLine strs
                 (h, d, a) = execMoves' cmds (0,0,0)
             in Just (h * d)

{-
-- 1484118 correct
-- 25 mins.
-- Started to play around with a nicer way to do the parse function but decided to just finish it
-- then come back to that after.
-}
main1 : IO()
main1 = withLines' "day02-input.txt" task1

{-
-- 1463827010 correct
-- 6 mins.
-- Easy to add an extra number to the state tuple.  If there are move attributes on the way
-- then should probably make it it's own type/record to keep track of things more easily.
-}
main2 : IO()
main2 = withLines' "day02-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
