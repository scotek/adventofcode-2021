module Day01

import System
import AOCUtils

import Data.Zippable
import Data.List

-- Standard zip of neighbouring pairs then count the numbers that have difference <0.
task1 : List Integer -> Maybe Nat
task1 [] = Nothing
task1 (x :: xs) = let pairs = zip (x :: xs) xs
                      diffs = map (uncurry (-)) pairs
                      negs = filter (<0) diffs
                      ans = length negs
                  in Just ans

-- Zip up 3 neighbours and sum them to create the sliding window values, then just do
-- task 1 with that list.
task2 : List Integer -> Maybe Nat
task2 [] = Nothing
task2 (x :: y :: zs) = let windows = zip3 (x::y::zs) (y::zs) zs
                           sums = map (\(a,b,c) => a + b + c) windows
                       in task1 sums
task2 (x :: xs) = Nothing

{-
To sate my desire to continue the uncurry approach used in task 1 from
a (a,b) tuple to a 3 element (a, b, c) tuple
[note (1,(2,3)) is the same as (1,2,3) in Idris],
this horrible combination gets the arguments in the correct order:
    sums = map (uncurry (flip ((+) . uncurry (+)))) windows

I think I'll stick with the lambda for readability(!)...and make my own infix
3-tuple function application function for the future (`uncurry3infix{l,r}`
in AOCUtils:
    sums = map (uncurry3infixl (+)) windows

Of course, could side-step all the that by just doing the ziping and summing
together but I like to keep them apart when doing puzzles like this for some reason
    sums = zipWith3 (\a,b,c => a+b+c) (x::y::zs) (y::zs) zs
-}

{-
-- 1162 correct
-- 30 mins (working out solution ~1 min, fighting with compiler ~29 :( )
-- Easy task but took forever as I hit a couple on unintuitive error messages
-- due to syntax errors on typos.
-}
main1 : IO()
main1 = withLines' "day01-input.txt" task1

{-
-- 1190 correct
-- Couple of minutes.
-- Easy modification by recognising it's the same task with the sliding window
-- totals, so just build them first then do task1.
-}
main2 : IO()
main2 = withLines' "day01-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
