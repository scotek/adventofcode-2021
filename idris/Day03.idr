module Day03

import System
import Data.List
import AOCUtils
import Data.Bits
import Debug.Trace


task1 : List String -> Maybe Int
task1 lines = let chrs = map unpack lines
                  tchrs = transpose chrs
                  sum1 = map (length . filter (=='1')) tchrs
                  gamma = map (\x => if x >= (length chrs) `div` 2 then '1' else '0') sum1
                  epsilon = map (\x => if x >= (length chrs) `div` 2 then '0' else '1') sum1
              in Just $ fromBitString (pack gamma) * fromBitString (pack epsilon)


criteriaFilter : (dat : List (List Char)) -- the grid of all the digits of all numbers
               -> (oneCounts : List Int)  -- how many 1's are in each column
               -> (col: Int)              -- the column we are looking at just now
               -> (op: Int -> Int -> Bool)-- we want the most or least common? (> or <)
               -> (boundaryMatch : Char)  -- if there are equals 1's and 0's, match this value
               -> List (List Char)
criteriaFilter dat oneCounts col op boundaryMatch
  = let oneCount = case inBounds (cast col) oneCounts of
                        Yes prf => (index (cast col) oneCounts)
                        _ => trace "count error" (-1) -- should do something more rigorous for this error
        keepVal = case cast oneCount == cast (length dat) - oneCount of
                       True => boundaryMatch
                       False => if oneCount `op` cast (cast (length dat) - oneCount) then '1' else '0'
        dat' = filter (filt keepVal) dat
    in dat'
    where filt : Char -> List Char -> Bool
          filt keepv ys = case inBounds (cast col) ys of
                               Yes prf => index (cast col) ys == keepv
                               _ => False


cFilter : (dat : List (List Char)) -> (Int->Int->Bool) -> (boundaryMatch : Char) -> (col : Int) -> List (List Char)
cFilter dat op boundaryMatch 12 = []
cFilter dat op boundaryMatch col = let tchrs = transpose dat
                                       sum1 = map (cast . length . filter (=='1')) tchrs
                                       newDat = criteriaFilter dat sum1 col op boundaryMatch
                                   in case (length newDat) == 1 of
                                           True => case inBounds 0 newDat of
                                                        Yes prf => [index 0 newDat]
                                                        _ => []
                                           False => cFilter newDat op boundaryMatch (col+1)



task2 : List String -> Maybe Int
task2 lines = let chrs = map unpack lines
                  oxygen = cFilter chrs (>) '1' 0
                  co2 = cFilter chrs (<) '0' 0
              in case inBounds 0 oxygen of
                      Yes prf => case inBounds 0 co2 of
                                 Yes prf' => Just $ fromBitString (pack (index 0 oxygen)) * fromBitString (pack (index 0 co2))
                                 _ => Nothing
                      _ => Nothing


{-
-- 2250414 correct
-- 2hrs.  Gave it another go learning the Data.Bits module this year.  Took ages to realise
-- casting a String to Bits16 doesn't actually interpret the "01010" as binary but
-- instead just treats it as a decimal number ((!?) as far as I can tell).  Gave it
-- a go implement my own bitstring to Bits16 function but there is no documentation
-- and I couldn't find an example of the module being used to help work out the type
-- hassle around `Index`.
-- 20min.  Just dealt with chars and did my own binary conversion.  Point of originally
-- trying to do it "properly" was anticipation of what could be in Part 2, so let's see...
-}
main1 : IO()
main1 = withLines' "day03-input.txt" task1

{-
-- 2793456 too low
-- 35 mins
-- Ah, was always dividing by 500 (half the input length) to work out more or less than half!
-- Needed to change it as the size of the list decreases during task 2.
-- Horrible code but couldn't be bothered changing it late at night while watching something
-- then I became more interested in why it didn't work than what the actual answer was.
-- Turns out doing part 1 with Bits16 wouldn't have saved me any time anyway.
-- Overall horrible code for both parts but since it's for fun I prefer to keep a record
-- of the bad approaches to see if there is anything common to improve on when it's all done.
-- All these `Nothing` deadends might look better with do notation; will try that at the next
-- useful point.
-}
main2 : IO()
main2 = withLines' "day03-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
