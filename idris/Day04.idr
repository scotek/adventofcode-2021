module Day04

import System
import AOCUtils
import Matrix
import Data.List
import Data.Vect
import Data.String
import Data.List1

--------------------------------------------------------------------------------
-- File parsing

parseHeader : List String -> (List Int, List String)
parseHeader [] = ([], [])
parseHeader (x :: xs) = let hdr = parseHead x
                        in (hdr, xs)
                        where parseHead : String -> List Int
                              parseHead str = forget $ map cast (split (==',') str)

parseBoards : List String -> List (Matrix 5 5 Int)
parseBoards (empty :: r1 :: r2 :: r3 :: r4 :: r5 ::strs) -- abuse pattern matching to grab lines
    = let d = (map words [r1, r2, r3, r4, r5])
          mb = toMatrixWith (cast {to=Int}) 5 5 d
          mb' = fromMaybe (replicateMatrix 5 5 (-1)) mb -- being naughty with a dummy error value
      in mb' :: parseBoards strs
parseBoards _ = []


parseFile : List String -> (List Int, List (Matrix 5 5 Int))
parseFile xs = let (hdr, xs') = parseHeader xs
                   brds = parseBoards xs'
               in (hdr, brds)

--------------------------------------------------------------------------------
-- Board representation

||| Represent a board state with a numbers matrix and a 'seen' matrix.
data Board = MkBoard (Matrix 5 5 Int) (Matrix 5 5 Bool)

||| Create a new board with no seen numbers.
makeBoard : Matrix 5 5 Int -> Board
makeBoard m = MkBoard m (replicateMatrix 5 5 False)

||| Give a board, mark the given number as seen if it is in the board, otherwise no change.
maybeMarkBoard : Board -> Int -> Board
maybeMarkBoard (MkBoard ns ms) n with (findIndex (==n) ns)
  maybeMarkBoard (MkBoard ns ms) n | Nothing = (MkBoard ns ms)
  maybeMarkBoard (MkBoard ns ms) n | (Just (r, c)) = (MkBoard ns (replaceAt ms r c True))

||| Does this board have a complete row or column?
hasWon : Board -> Bool
hasWon (MkBoard ns ms) = let rs = foldRows (delay .: (&&)) True ms
                             cs = foldColumns (delay .: (&&)) True ms
                             rs' = or rs
                             cs' = or cs
                         in rs' || cs'

||| Are any of the boards in the list winning?
anyWon : List Board -> Maybe Board
anyWon [] = Nothing
anyWon (x :: xs) = if hasWon x then Just x else anyWon xs

||| Return a list of all unseen numbers on this board.
unmarkedNumbers : Board -> List (Int)
unmarkedNumbers (MkBoard xs ys) = let xs' = Data.Vect.concat xs
                                      ys' = Data.Vect.concat ys
                                      combined = zip xs' ys'
                                      filtered = snd $ filter (not . snd) combined
                                      filtered' = map Builtin.fst filtered
                                  in toList filtered'


--------------------------------------------------------------------------------
-- Task 1 game

||| Play the game until a single board wins or run out of moves
playGame : List Int -> List Board -> Maybe Int
playGame [] ys = Nothing
playGame (x :: xs) ys = let ys' = map (flip maybeMarkBoard x) ys
                        in case anyWon ys' of
                                Nothing => playGame xs ys'
                                Just winner => Just $ sum (unmarkedNumbers winner) * x



task1 : List String -> Maybe Nat
task1 lines = let (hdr, brds) = parseFile lines
                  brds' = map makeBoard brds
              in case playGame hdr brds' of
                      Nothing => Nothing
                      Just n => Just $ cast n
                      
--------------------------------------------------------------------------------
-- Task 2 game

||| Remove any board from the list that has won
removeWon : List Board -> List Board
removeWon [] = []
removeWon (x :: xs) = if hasWon x then removeWon xs else x :: removeWon xs

||| Play the game, discarding winning boards, until the last remaining board wins.
||| We need to know which number causes the last board to win (to calc the score) so
||| we can't match on a single element of `ys` at the top level since that would be when
||| the 2nd to last board wins, not the last one.
playGame' : List Int -> List Board -> Maybe Int
playGame' [] ys = Nothing
playGame' (x :: xs) ys = let ys' = map (flip maybeMarkBoard x) ys
                        in case removeWon ys' of
                                [] => case inBounds 0 ys' of
                                           No _ => Nothing
                                           Yes prf => Just $ sum (unmarkedNumbers (index 0 ys')) * x
                                zs =>  playGame' xs zs



task2 : List String -> Maybe String
task2 lines = let (hdr, brds) = parseFile lines
                  brds' = map makeBoard brds
              in case playGame' hdr brds' of
                      Nothing => Nothing
                      Just n => Just $ cast n

{-
-- 591 too low (forgot multiply)
-- 8274 too low (was summing the seen numbers not the unseen ones)
-- 8442 correct
-- 2h 31m  build generic matrix module and parsing
-- 7h 8m   board and actual bingo while distracted with dinner + videos
-}
main1 : IO()
main1 = withLines' "day04-input.txt" task1

{-
-- 4590 correct (playing around with different ways to nicely remove winning boards,
--              wrong recursive case didn't remove multiple winning boards in the same turn).
-- 34m
--
-- Today's problem is exactly the type of problem I hate to do in Idris.  There is so much more
-- I know about the state of the data as it's flowing through the program that is incredibly annoying
-- to have to spell out to the language that I feel punished for trying to take advantage of known
-- invariants about whatever point in the flow I'm working on because the compiler insists I can't
-- 'skip' checks or make assumptions.  There are also endless random tangents you have to go down
-- because you've bumped into some new weirdness (today it was the Lazy type on &&,||,or suddenly
-- needing to be dealt with explicitly).  Unlike previous years, I'm deliberately trying not to
-- take shortcuts this year (or switch to Haskell) to try to force myself to iron out theose creases
-- for the future but it's sucking the fun out of it given how little free time I have at this
-- point of the year.
-}
main2 : IO()
main2 = withLines' "day04-input.txt" task2

||| Main driver.  Will run task 1 or 2 depending on command line args.
||| The `main1` and `main2` functions are more convenient to use interactively.
main : IO()
main = do args <- getArgs
          case inBounds 1 args of
            Yes prf => case (index 1 args) == "-1" of
                            True => main1
                            False => main2
            No _ =>  do main1
                        main2
